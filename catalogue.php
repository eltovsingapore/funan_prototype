			<div class="inner-catalogue-body"> 
				<div class="show-sample">
					<a class="btn btn-danger btn-design1"> 
						Design 1
					</a>
				</div>
				<div class="sample design1" style="display: none;">
					<img src="./asset/catalogue/catalogue_v2.jpg" />
				</div>

				<div class="show-sample" style="top: 160px;">
					<a class="btn btn-danger btn-design2"> 
						Design 2
					</a>
				</div>
				<div class="sample design2" style="display: none; margin-top: 40px;">
					<img src="./asset/shops/product_v2.jpg" />
				</div>	

				<div class="slider">
					<!-- First Slide -->
					<div class="slider-target">
						<div class="container"> 
							<div class="row odd popup-btn"> 
								<div class="col-6 left">
									<p class="location"> #01 - 05 </p>
									<img src="./asset/catalogue/reneevon.jpg" class="store-image"/>
									<img src="./asset/catalogue/qr.jpg" class="align-bottom qr"/>
								</div>
								<div class="col-6 right">
									<h2 class="text-uppercase mb-3"> Reneevon  </h2>
									<p> Renibon pursues a romantic feeling that a girl dreams of and a classy dignity that can be felt in a noble family. It offers a modern British romanticism with modern sensitivity and high quality.</p> 
									<p> Fall temperature Renonbone, and fall. Up to 15%+5% secret.</p>
								</div>
							</div>
							<div class="row even popup-btn"> 
								<div class="col-6 right text-right">
									<h2 class="text-uppercase mb-3"> Issey miyake  </h2>
									<p> AUTUMN WINTER 2018 Season with the spiral of life, we search for the answer, spending many nights, throughout a long winter</p> 
									<p> More gallantly than the forsty wind, more determined than a beast waiting for the spring.</p>
								</div>
								<div class="col-6 left">
									<img src="./asset/catalogue/qr.jpg" class="align-bottom qr"/>
									<img src="./asset/catalogue/issey_miyake.jpg" class="store-image"/>
									<p class="location"> #01 - 06 </p>
								</div>
							</div>
							<div class="row odd popup-btn"> 
								<div class="col-6 left">
									<p class="location"> #01 - 07 </p>
									<img src="./asset/catalogue/olive_des_olive.jpg" class="store-image"/>
									<img src="./asset/catalogue/qr.jpg" class="align-bottom qr"/>
								</div>
								<div class="col-6 right">
									<h2 class="text-uppercase mb-3"> Olive Des Olive </h2>
									<p> A purveyor of luxury, heritage inspired apparel and a lifestyle destination for the discerning gentleman. We make clothes that speak style and personality. We start with our shirts, an eclectic mix that marry versatility and style- a display of sartorial elegance that leans towards desing.</p>
								</div>
							</div>
							<div class="row even popup-btn"> 
								<div class="col-6 right text-right">
									<h2 class="text-uppercase mb-3"> Deco  </h2>
									<p> In pursuit of depth, seeking the lifestyle of an intelligent modern woman with an authentic and elegant attitude that does not show off.</p> 
									<p> Classic & basic style that can be worn over time, pratical, rational and value oriented.</p>
								</div>
								<div class="col-6 left">
									<img src="./asset/catalogue/qr.jpg" class="align-bottom qr"/>
									<img src="./asset/catalogue/deco.jpg" class="store-image"/>
									<p class="location"> #01 - 08 </p>
								</div>
							</div>
						</div>
					</div>
					<!-- Second Slide -->
					<div class="slider-target">
						<div class="container"> 
							<div class="row odd popup-btn"> 
								<div class="col-6 left">
									<p class="location"> #01 - 05 </p>
									<img src="./asset/catalogue/reneevon.jpg" class="store-image"/>
									<img src="./asset/catalogue/qr.jpg" class="align-bottom qr"/>
								</div>
								<div class="col-6 right">
									<h2 class="text-uppercase mb-3"> Reneevon  </h2>
									<p> Renibon pursues a romantic feeling that a girl dreams of and a classy dignity that can be felt in a noble family. It offers a modern British romanticism with modern sensitivity and high quality.</p> 
									<p> Fall temperature Renonbone, and fall. Up to 15%+5% secret.</p>
								</div>
							</div>
							<div class="row even popup-btn"> 
								<div class="col-6 right text-right">
									<h2 class="text-uppercase mb-3"> Issey miyake  </h2>
									<p> AUTUMN WINTER 2018 Season with the spiral of life, we search for the answer, spending many nights, throughout a long winter</p> 
									<p> More gallantly than the forsty wind, more determined than a beast waiting for the spring.</p>
								</div>
								<div class="col-6 left">
									<img src="./asset/catalogue/qr.jpg" class="align-bottom qr"/>
									<img src="./asset/catalogue/issey_miyake.jpg" class="store-image"/>
									<p class="location"> #01 - 06 </p>
								</div>
							</div>
							<div class="row odd popup-btn"> 
								<div class="col-6 left">
									<p class="location"> #01 - 07 </p>
									<img src="./asset/catalogue/olive_des_olive.jpg" class="store-image"/>
									<img src="./asset/catalogue/qr.jpg" class="align-bottom qr"/>
								</div>
								<div class="col-6 right">
									<h2 class="text-uppercase mb-3"> Olive Des Olive </h2>
									<p> A purveyor of luxury, heritage inspired apparel and a lifestyle destination for the discerning gentleman. We make clothes that speak style and personality. We start with our shirts, an eclectic mix that marry versatility and style- a display of sartorial elegance that leans towards desing.</p>
								</div>
							</div>
							<div class="row even popup-btn"> 
								<div class="col-6 right text-right">
									<h2 class="text-uppercase mb-3"> Deco  </h2>
									<p> In pursuit of depth, seeking the lifestyle of an intelligent modern woman with an authentic and elegant attitude that does not show off.</p> 
									<p> Classic & basic style that can be worn over time, pratical, rational and value oriented.</p>
								</div>
								<div class="col-6 left">
									<img src="./asset/catalogue/qr.jpg" class="align-bottom qr"/>
									<img src="./asset/catalogue/deco.jpg" class="store-image"/>
									<p class="location"> #01 - 08 </p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>


		<script type="text/javascript">
			$('.btn-design1').click(function(){
				if( !$(this).hasClass('active') ){
					$('.show-sample .btn').removeClass('active');
					$(this).addClass('active');
					$('.sample').removeClass('slideInRight animated').addClass('slideOutRight animated').fadeOut(1000);
					$('.sample.design1').removeClass('slideOutRight animated').css('display', 'block').addClass('slideInRight animated');
				}else{
					$('.show-sample .btn').removeClass('active');
					$('.sample').removeClass('slideInRight animated').addClass('slideOutRight animated').fadeOut(1000);
				}
			});

			$('.btn-design2').click(function(){
				if( !$(this).hasClass('active') ){
					$('.show-sample .btn').removeClass('active');
					$(this).addClass('active');
					$('.sample').removeClass('slideInRight animated').addClass('slideOutRight animated').fadeOut(1000);
					$('.sample.design2').removeClass('slideOutRight animated').css('display', 'block').addClass('slideInRight animated');
				}else{
					$('.show-sample .btn').removeClass('active');
					$('.sample').removeClass('slideInRight animated').addClass('slideOutRight animated').fadeOut(1000);
				}
			});
		</script>