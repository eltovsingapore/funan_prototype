	<div class="floor-map-container">
		<div id="l4" class="map"> 
			<img src="./asset/floor/l4.jpg" />
		</div>
	</div>

	<div class="direction-block row"> 
		<div class="col-3">
			<img src="./asset/floor/zoom.jpg" />	
			<img src="./asset/floor/rotate.jpg" />	
			<img src="./asset/floor/angle.jpg" />	
		</div>
		<div class="col-6 text-center"> 
			<h1 class="align-middle"> Your Location (L4) </h1>
		</div>
		<div class="col-3">
			<div class="text-center" data-target="reset" style="float: right;"> 
				<img src="./asset/floor/reset.jpg" />
				<p> Reset </p>
			</div>
		</div>
	</div>

	<hr class="light"> 

	<div class="sign-block mt-5 mb-5">
		<!-- First Sign -->
		<div class="icon" data-sing="shower"> 
			<img src="./asset/floor/shower.jpg" />
			<p>Shower room</p>
		</div>
		<div class="icon" data-sing="baby"> 
			<img src="./asset/floor/baby.jpg" /> 
			<p> Baby room </p>
		</div>
		<div class="icon" data-sing="dropoff"> 
			<img src="./asset/floor/dropoff.jpg" /> 
			<p>Drop off point</p>
		</div>
		<div class="icon" data-sing="taxi"> 
			<img src="./asset/floor/taxi.jpg" /> 
			<p> Taxi stand </p>
		</div>
		<div class="icon" data-sing="bicycle"> 
			<img src="./asset/floor/bicycle.jpg" /> 
			<p> Bicycle parking </p>
		</div>
		<div class="icon" data-sing="toilet"> 
			<img src="./asset/floor/toilet.jpg" /> 
			<p> Toilet </p>
		</div>
		<div class="icon" data-sing="retail"> 
			<img src="./asset/floor/retail.jpg" /> 
			<p> Retail Lift </p>
		</div>
		<div class="icon" data-sing="office"> 
			<img src="./asset/floor/office.jpg" /> 
			<p> Office Lift </p>
		</div>
		<div class="icon" data-sing="apartment"> 
			<img src="./asset/floor/apartment.jpg" /> 
			<p> Apartment Lift </p>
		</div>
		<!-- Second Sign -->
		<div class="icon" data-sing="shower"> 
			<img src="./asset/floor/shower.jpg" />
			<p>Shower room</p>
		</div>
		<div class="icon" data-sing="baby"> 
			<img src="./asset/floor/baby.jpg" /> 
			<p> Baby room </p>
		</div>
		<div class="icon" data-sing="dropoff"> 
			<img src="./asset/floor/dropoff.jpg" /> 
			<p>Drop off point</p>
		</div>
		<div class="icon" data-sing="taxi"> 
			<img src="./asset/floor/taxi.jpg" /> 
			<p> Taxi stand </p>
		</div>
		<div class="icon" data-sing="bicycle"> 
			<img src="./asset/floor/bicycle.jpg" /> 
			<p> Bicycle parking </p>
		</div>
		<div class="icon" data-sing="toilet"> 
			<img src="./asset/floor/toilet.jpg" /> 
			<p> Toilet </p>
		</div>
		<div class="icon" data-sing="retail"> 
			<img src="./asset/floor/retail.jpg" /> 
			<p> Retail Lift </p>
		</div>
		<div class="icon" data-sing="office"> 
			<img src="./asset/floor/office.jpg" /> 
			<p> Office Lift </p>
		</div>
		<div class="icon" data-sing="apartment"> 
			<img src="./asset/floor/apartment.jpg" /> 
			<p> Apartment Lift </p>
		</div>
	</div>

	<div class="floor-level"> 
		<div class="floor-block" data-floor="#b4"> B4 </div>
		<div class="floor-block" data-floor="#b3"> B3 </div>
		<div class="floor-block" data-floor="#b2"> B2 </div>
		<div class="floor-block" data-floor="#b1"> B1 </div>
		<div class="floor-block" data-floor="#l1"> L1 </div>
		<div class="floor-block" data-floor="#l2"> L2 </div>
		<div class="floor-block" data-floor="#l3"> L3 </div>
		<div class="floor-block active" data-floor="#l4"> L4 </div>
		<div class="floor-block" data-floor="#l5"> L5 </div>
		<div class="floor-block" data-floor="#l7"> L7 </div>
	</div>