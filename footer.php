<!-- Modal -->
<div class="modal fade" id="shop-popup" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
			</div>
			<div class="modal-body">
				<p class="popup-title"> Fashion / Sports </p>
				<h2 class="modal-title">Adidas Performance</h2>
				<div class="popup-body">
					<img src="./asset/catalogue/popup-image.jpg" class="d-block img-responsive mt-5 mb-5" id="popup-main"/>
					<div class="popup-sub-images clearfix">
						<img src="./asset/shops/sub-images.png" class="d-block img-responsive mt-5 mb-5"/>
					</div>
					<div class="popup-body-description">
						<p> In pursuit of depth, seeking the lifestyle of an intelligent modern woman with an authentic and elegant attitude that does not show off.</p> 
						<p> Classic & basic style that can be worn over time, pratical, rational and value oriented.</p>
						<br>
						<div class="left"> <img src="./asset/icons/location.jpg" class="inner-icon"/> <span> #03-01 </span> </div>
						<div class="right"><img src="./asset/icons/phone.jpg" class="inner-icon"/> <span> 6123-1234 </span> </div>
						<div class="popup-body-footer mt-5">
							<div class="funan-btn popup-btn btn-directory-blue" id="show-salomon"> Direction </div> 
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</div>
	</div>
</div>

<footer>
	<div class="text-center search-box"> 
		<input type="text" name="search" id="input-search" placeholder="Brand Search" class="text-center" />
		<div class="search-icon"> <img src="./asset/icons/search.jpg" /></div>
		<div class="keyboard-wrapper">
			<div id="keyboard"> </div>
			<div class="btn-close-keyboard"> x </div>
		</div>
	</div>
	<div class="container" id="footer-top"> 
		<div class="funan-row clearfix text-center mb-4">
			<a class="fr-btn"> 
				<img src="./asset/icons/fr-icon.jpg"> 
				<p class="d-inline"> Face Recognition </p>
			</a>
		</div>
	</div>
	<hr>
	<div class="container" id="footer-bottom">
		<div class="funan-row clearfix">
			<div class="menu-block home active"> 
				<img src="./asset/icons/home.png" /> 
				<p> Home </p>
			</div>
			<div class="menu-block floor-guide"> 
				<img src="./asset/icons/floor-guide.png" /> 
				<p> Floor Guide </p>
			</div>
			<div class="menu-block directory"> 
				<img src="./asset/icons/directory.png" /> 
				<p> Directory </p>
			</div>
			<div class="menu-block catalogue"> 
				<img src="./asset/icons/catalogue.png" /> 
				<p> Catalogue </p>
			</div>
			<div class="menu-block happening"> 
				<img src="./asset/icons/happening.png" /> 
				<p> Happenings </p>
			</div>
			<div class="menu-block transport"> 
				<img src="./asset/icons/transport.png" /> 
				<p> Transport </p>
			</div>
			<div class="menu-block whatsplaying"> 
				<img src="./asset/icons/whatsplaying.png" />
				<p> What's Playing </p>
			</div>
		</div>
	</div>

</footer>
</div>
</body>
</html>