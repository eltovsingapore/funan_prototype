	<div class="inner-fr-title text-center"> 
		<h1> Hello, Welcome! </h1>
		<h3> Suggested events and products for you </h3>
	</div>
	<div class="inner-fr-top"> 
		<div class="logo">
			<img src="./asset/fr/yankee.jpg">
			<img src="./asset/fr/ud.jpg">
			<img src="./asset/fr/yacht.jpg">
			<img src="./asset/fr/aldo.jpg">
			<img src="./asset/fr/uniqlo.jpg">
			<img src="./asset/fr/nubox.jpg">
			<img src="./asset/fr/ace.jpg">
			<img src="./asset/fr/yankee.jpg">
			<img src="./asset/fr/ud.jpg">
			<img src="./asset/fr/yacht.jpg">
			<img src="./asset/fr/aldo.jpg">
			<img src="./asset/fr/uniqlo.jpg">
			<img src="./asset/fr/nubox.jpg">
			<img src="./asset/fr/ace.jpg">
		</div>
	</div>
	
	<div class="inner-fr-content">
		<div class="row mb-5">
			<div class="col-6">
				<img src="./asset/fr/main-lady.jpg"/>
			</div>
			<div class="col-6 description">
				<h3> A Jurassic World:</h3>
				<h3> Fallen Kingdom Adventure</h3>
				<br>
				<h5>31 May 2018 - 30 Jun 2018</h5>
				<br>
				<p> Venue: level 1, Main Atrium </p>
				<p> Life-size sculpture of the velociraptor - Blue </p>
				<p> Immersive VR Experience & Gaming Zone </p>
				<p> ^Sandpit & Workshops </p>
				<p> ^Redeem one entry for either one of the activities with min. $30 spend in a single receipt. Redeem an entry for two when changed to American Express CapitaCard. Redemption can be made at Plaza Singapura New Extension, Unit#03-76 [workshop]/Main Atrium [Sandpit]. Redemption for workshop entry is limit to first 80 per day. ^Each entry admits one pax, max 4 entry passes per receipt.</p>
				<br> 
				<p> ^Receipt can only be used ONCE for either the Sandpit or Workshop.</p>
			</div>
		</div>

		<div class="inner-fr-bottom">
			<div class="row" style="margin-bottom: 60px !important;">
				<div class="col-6">
					<div class="row">
						<div class="col-6">
							<img src="./asset/fr/pleats.jpg" />
						</div>
						<div class="col-6"> 
							<img src="./asset/fr/hazzys.jpg" />
						</div>
					</div>
				</div>
				<div class="col-6"> 
					<div class="row">
						<div class="col-6"> 
							<img src="./asset/fr/cressonn.jpg" />
						</div>
						<div class="col-6"> 
							<img src="./asset/fr/tandy.jpg" />
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-6"> </div>
				<div class="col-6">
					<video width="100%" height="260" autoplay muted="muted" loop>
						<source src="./asset/fr/fr-food.mp4" type="video/mp4">
							Your browser does not support the video tag.
						</video>
						<img src="./asset/fr/video-thumbnail-1.jpg" class="d-none"/>
					</div>
				</div>
			</div>
		</div>