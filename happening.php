
<img src="./asset/bg/front-layer.png" class="top-layer" /> 
<div class="inner-happening-body"> 

	<div id="discover-new" class="active slider-target">
		<div class="slider">
			<div class="slider-box">
				<div class="image-wrapper clearfix mb-5">
					<img src="./asset/whatshappening/whatshappening.jpg" />
				</div>

				<h1> Wild Rice </h1>
				<p class="sub-title"> 380 Seats live performance Theatre </p>
				<br>
				<h3> Cultivating Culture and Art </h3>
				<br>
				<p> Singapore's leading professional theatre company elevates the createive spirtit of Funan community with its dynamic presence.</p>
				<br>
				<p> 
					"in order to understand our world, to effect change, to respond to issues with compassion and integrity; our society must be able to see beyond the limitations and inevitable biases of our own prespectives. We need to imagine other possibilities. We need a larger vision. We need art, because the vision to see other possibilities is the gift art gives to society. And live performace gives art a connecting energy parallel to none."
				</p>
				<br> 
				<p> Founding Artistic Director, Ivan Heng </p>
			</div>
			<div class="slider-box">
				<div class="image-wrapper clearfix mb-5">
					<img src="./asset/whatshappening/whatshappening_mall.jpg" />
				</div>

				<h1> Wild Rice </h1>
				<p class="sub-title"> 380 Seats live performance Theatre </p>
				<br>
				<h3> Cultivating Culture and Art </h3>
				<br>
				<p> Singapore's leading professional theatre company elevates the createive spirtit of Funan community with its dynamic presence.</p>
				<br>
				<p> 
					"in order to understand our world, to effect change, to respond to issues with compassion and integrity; our society must be able to see beyond the limitations and inevitable biases of our own prespectives. We need to imagine other possibilities. We need a larger vision. We need art, because the vision to see other possibilities is the gift art gives to society. And live performace gives art a connecting energy parallel to none."
				</p>
				<br> 
				<p> Founding Artistic Director, Ivan Heng </p>
			</div>
		</div>
	</div>

	<div id="mall" class="slider-target">
		<div class="slider">
			<div class="slider-box">
				<div class="image-wrapper clearfix mb-5">
					<img src="./asset/whatshappening/whatshappening_mall.jpg" />
				</div>

				<h1> Wild Rice </h1>
				<p class="sub-title"> 380 Seats live performance Theatre </p>
				<br>
				<h3> Cultivating Culture and Art </h3>
				<br>
				<p> Singapore's leading professional theatre company elevates the createive spirtit of Funan community with its dynamic presence.</p>
				<br>
				<p> 
					"in order to understand our world, to effect change, to respond to issues with compassion and integrity; our society must be able to see beyond the limitations and inevitable biases of our own prespectives. We need to imagine other possibilities. We need a larger vision. We need art, because the vision to see other possibilities is the gift art gives to society. And live performace gives art a connecting energy parallel to none."
				</p>
				<br> 
				<p> Founding Artistic Director, Ivan Heng </p>
			</div>
			<div class="slider-box">
				<div class="image-wrapper clearfix mb-5">
					<img src="./asset/whatshappening/whatshappening.jpg" />
				</div>

				<h1> Wild Rice </h1>
				<p class="sub-title"> 380 Seats live performance Theatre </p>
				<br>
				<h3> Cultivating Culture and Art </h3>
				<br>
				<p> Singapore's leading professional theatre company elevates the createive spirtit of Funan community with its dynamic presence.</p>
				<br>
				<p> 
					"in order to understand our world, to effect change, to respond to issues with compassion and integrity; our society must be able to see beyond the limitations and inevitable biases of our own prespectives. We need to imagine other possibilities. We need a larger vision. We need art, because the vision to see other possibilities is the gift art gives to society. And live performace gives art a connecting energy parallel to none."
				</p>
				<br> 
				<p> Founding Artistic Director, Ivan Heng </p>
			</div>
		</div>
	</div>

	<div class="tab-menu-box mt-5">
		<div class="row border">
			<div class="col-6 tab-block active" data-menubox="category-menu-box">
				<a class="text-center" data-target="#discover-new"> DISCOVER NEW FUNAN </a>
			</div>
			<div class="col-6 tab-block" style="margin-left: 6px;">
				<a class="text-center" data-target="#mall"> MALL </a>
			</div>
		</div>
	</div>
</div>