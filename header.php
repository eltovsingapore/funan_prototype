<!DOCTYPE html>
<html>
<head>
	<title>Funan Project v.1</title>

	<!-- jQuery -->
	<script type="text/javascript" src="./components/jquery-3.3.1.min.js"></script>

	<!-- bootstrap css -->
	<link rel="stylesheet" href="./components/bootstrap/css/bootstrap-reboot.min.css">
	<link rel="stylesheet" href="./components/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="./components/bootstrap/css/bootstrap-grid.min.css">

	<!-- bootstrap js -->
	<script type="text/javascript" src="./components/bootstrap/js/bootstrap.min.js"></script>

	<!-- Moment.js -->
	<script type="text/javascript" src="./components/moment.js"></script>

	<!-- Keyboard JS -->
	<link href="./components/keyboard/css/jkeyboard.css" rel="stylesheet">
	<script src="./components/keyboard/js/jkeyboard.js"></script>

	<!-- Flickity JS -->
	<link href="./components/flickity/flickity.css" rel="stylesheet">
	<script src="./components/flickity/flickity.pkgd.min.js"></script>

	<!-- Animate Css -->
	<link href="./components/animate.css" rel="stylesheet">

	<!-- Funan custom JS -->
	<script type="text/javascript" src="./js/main.js"></script>
	<!-- Funan custom css -->
	<link rel="stylesheet" href="./style/funanstyle.css">

</head>
<body class="">
	<div id="funan-body">
		<header> 
			<div class="container">
				<div class="row">
					<div class="col-6" id="page-title">
					</div>
					<div class="col-6" id="header-right">
						<div class="row">
							<div class="col-5 d-flex">
								<img src="./asset/icons/clock.png" />
								<div class="d-inline-block block">
									<p class="font-color"> <span class="font-sm"> Operating Hour </span> </p>
									<p> 10am to 10pm</p>
								</div>
							</div>
							<div class="col-7 d-flex border-left">
								<img src="./asset/icons/wifi.png" />
								<div class="d-inline-block block">
									<p class="font-color"> <span class="font-sm"> Network ID </span> </p>
									<p> FUNAN </p>
								</div>
								<div class="d-inline-block block">
									<p class="font-color"> <span class="font-sm"> Password </span> </p>
									<p> FREEWIFI </p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</header>