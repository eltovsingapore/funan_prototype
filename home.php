
<div class="home-header"> 
	<div class="clearfix">
		<img src="./asset/general/logo.jpg" id="logo"/>
	</div>
	<div class="row clearfix">
		<div class="col-6"> 
			<span id="date">  </span>
			<span id="time">  </span>
		</div>
		<div class="col-6" id="header-right">
			<div class="row">
				<div class="col-5 d-flex">
					<img src="./asset/icons/clock.png" />
					<div class="d-inline-block block">
						<p class="font-color"> <span class="font-sm"> Operating Hour </span> </p>
						<p> 10am to 10pm</p>
					</div>
				</div>
				<div class="col-7 d-flex border-left">
					<img src="./asset/icons/wifi.png" />
					<div class="d-inline-block block">
						<p class="font-color"> <span class="font-sm"> Network ID </span> </p>
						<p> FUNAN </p>
					</div>
					<div class="d-inline-block block">
						<p class="font-color"> <span class="font-sm"> Password </span> </p>
						<p> FREEWIFI </p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="home-slider"> 
	<img src="./asset/banners/home-slider.jpg" />
</div>


<div class="home-content">
	<h1 class="text-uppercase"> Welcome to funan </h1> <br>
	<p> SINGAPORE'S NEW CREATIVE INTERSECTION WHERE LIVE, WORK AND PLAY CONVERGE.</p>
	<p> COMING UP IN 4Q 2019, FUNAN WILL BE A LIFESTYLE DESTINATION THAT ENABLES</p>
	<p> SHOPPERS TO ENJOY A MYRIAD OF EXPERIENCES IN LINE WITH THEIR INTERESTS.</p>
</div>