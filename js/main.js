function home(){
	// home page functions
	$('.home-header #date').html(moment().format('dddd,D MMMM YYYY'));
	function update() {
		$('.home-header #time').html(moment().format('LT'));
	}
	setInterval(update, 1000);
}

function directory(){
	// Directory Page Functions
	$('#content-top-container').flickity({
		cellAlign: 'center',
		contain: true,
		prevNextButtons: false,
		pageDots: true
	});

	$('.popup-btn').click(function(e){
		e.preventDefault();
		var title = $(this).find('.shop-title').html();
		var location = $(this).find('.shop-location').html();
		$('.modal-title').html(title);
		$('.popup-body-description .left span').html(location);
		$('.popup-body #popup-main').attr('src', './asset/general/popup-image.jpg');
		$('#shop-popup').modal('show');
	});

	$('#category-menu-box .category-icon').click(function(){
		if( !$(this).hasClass('active') ){
			$('#category-menu-box .category-icon').removeClass('active');
			$(this).find('img').removeClass('fadeIn animated');
			$(this).addClass('active');
			$(this).find('img').addClass('fadeIn animated');
			// $('.tag-box').removeClass('active slideInUp animated');
			$('.tag-box').removeClass('active');
			var target = '#'+$(this).data('tagbox');
			// $(target).addClass('active slideInUp animated');
			$(target).addClass('active');
		}
	});

	$('#content-bottom-container .tab-block').click(function(){
		if( !$(this).hasClass('active') ){
			$('#content-bottom-container .tab-block').removeClass('active');
			$(this).addClass('active');
			var target = '#'+$(this).data('menubox');
			$('[data-menu="menu"]').removeClass('active slideInUp animated').delay(3000).addClass('slideOutUp animated');
			$(target).removeClass('slideOutUp animated').delay(3000).addClass('active slideInUp animated');
		}
	});

	$('.tag-box a').click(function(){
		if( !$(this).hasClass('active') ){
			$('.tag-box a').removeClass('active');
			$(this).addClass('active');
		}
	});

	$('.floor-block').click(function(){
		if( !$(this).hasClass('active') ){
			$('.floor-block').removeClass('active');
			$('.floor-block a').removeClass('bounceIn animated');
			$(this).find('a').addClass('bounceIn animated');
			$( $(this).addClass('active'));
		}
	});
	$("#page-title").html("<h1 class='outline-font'> Directory </h1>");
}

function transport(){
	//Transport Page Functions
	$('.transport-page .tab-block').click(function(){
		if( !$(this).hasClass('active') ){
			$('.transport-page .tab-block').removeClass('active');
			var target = $(this).data('target');
			console.log(target);
			$('.tab-body').hide();
			$(target).show();
			$(this).addClass('active');
		}
	});

	$("#page-title").html("<h1 class='outline-font'> Transport </h1>");

	$('#carpark-keyboard').jkeyboard({
		layout: "english_carpark",
		input: $('#carpark-input-search'),
		customLayouts: {
			selectable: ["english_carpark"],
			english_carpark: [
			['1', '2', '3', '4', '5', 'backspace'],
			['6', '7', '8', '9', '0', 'return' ]
			],
		}
	});

	$('#carpark-keyboard .jkey.return').click(function(){
		var value = $('#carpark-input-search').val();
		console.log(value);
		if( value ){
			$('#carpark-info').hide();
			$('#result-slider').show();
			$('#result-slider .slider').flickity({
				cellAlign: 'center',
				contain: true,
				prevNextButtons: false,
				pageDots: true
			});
			$('#result-slider .slider').flickity('resize');

		}else{
			$('#carpark-info').show();
			$('#result-slider').hide();
		}
	})
	
}

function happening(){
	$('.happening-page .tab-block').click(function(){
		if( !$(this).hasClass('active') ){
			$('.happening-page .tab-block').removeClass('active');
			$(this).addClass('active');

			var target = $(this).find('a').data('target');
			$('.slider-target').removeClass('active');
			$(target).addClass('active');

			// Rebuild Flickity
			var carousel = $(target).find('.slider').flickity({
				cellAlign: 'center',
				contain: true,
				prevNextButtons: false,
				pageDots: true
			});
			carousel.flickity('resize');
		}
	});

	$('#discover-new .slider').flickity({
		cellAlign: 'center',
		contain: true,
		prevNextButtons: false,
		pageDots: true
	});
	$('#discover-new .slider').flickity('resize');
	$("#page-title").html("<h1 class='outline-font'> Happening </h1>");
}

function catalogue(){
	$('.catalogue-page .slider').flickity({
		contain: true,
		prevNextButtons: false,
		pageDots: true
	});
	$('.catalogue-page .slider').flickity('resize');

	$('.popup-btn').click(function(){
		var title = $(this).find('h2').html();
		var location = $(this).find('.location').html();
		$('.modal-title').html(title);
		$('.popup-body-description .left span').html(location);
		$('.popup-body popup-image').attr('src', './asset/catalogue/popup-image.jpg');
		$('#shop-popup').modal('show');
	});
	$("#page-title").html("<h1 class='outline-font'> Catalogue </h1>");
}

function fr(){
	$('.inner-fr-top .logo').flickity({
		autoPlay: 3000,
		contain: true,
		prevNextButtons: false,
		pageDots: false,
		// setGallerySize: false
	});
	$('.inner-fr-top .logo').flickity('resize');
}

function floorguide(){
	$('.sign-block').flickity({
		contain: true,
		prevNextButtons: true,
		pageDots: false,
		groupCells: 1
	});
	$('.sign-block').flickity('resize');

	// Floor Level Menu
	$('.floor-level .floor-block').click(function(){
		if( !$(this).hasClass('active') ){
			$('.floor-level .floor-block').removeClass('active');
			$(this).addClass('active');
		}
	});
	$("#page-title").html("<h1 class='outline-font'> L4 </h1> <p> Women's Fashion / Goods / Cosmetics </p> ");
}

$( document ).ready(function() {
	//Ajax Page Changes Functions 
	//Home Menu
	$('.menu-block.home').on('click', function(){
		if( !$('#inner-main-container').hasClass('home-page') ){
			$("#inner-main-container").removeClass('fadeIn animated').addClass('fadeOut animated');
			$.ajax({
				url: "home.php",
				success: function(data){
					setTimeout(function() {
						$("#inner-main-container").html(data);
						$("#inner-main-container").removeClass().addClass('home-page fadeIn animated');
						$('body').removeClass().addClass('home');
					// $('header').slideUp();
					home();
				}, 1000);
				}
			});
		}
	});
	// FR btn
	$('.fr-btn').on('click', function(){
		if( !$('#inner-main-container').hasClass('fr-page') ){
			$("#inner-main-container").removeClass('fadeIn animated').addClass('fadeOut animated');
			$('footer .menu-block').removeClass('active');
			$('footer .menu-block.home').addClass('active');
			$.ajax({
				url: "fr.php",
				success: function(data){
					setTimeout(function() {
						$("#inner-main-container").html(data);
						$("#inner-main-container").removeClass().addClass('fr-page fadeIn animated');
						$('body').removeClass().addClass('fr');
						fr();
					}, 1000);
				}
			});
		}
	});
	//Transport Menu
	$('.menu-block.transport').on('click', function(){
		if( !$('#inner-main-container').hasClass('transport-page') ){
			$("#inner-main-container").removeClass('fadeIn animated').addClass('fadeOut animated');
			$.ajax({
				url: "transport.php",
				success: function(data){
					setTimeout(function() {
						$("#inner-main-container").html(data);
						$("#inner-main-container").removeClass().addClass('transport-page fadeIn animated');
						$('body').removeClass().addClass('transport');
						transport();
					}, 1000);
				}
			});
		}
	});
	//Floor Guide
	$('.menu-block.floor-guide').on('click', function(){
		if( !$('#inner-main-container').hasClass('floorguide-page') ){
			$("#inner-main-container").removeClass('fadeIn animated').addClass('fadeOut animated');
			$.ajax({
				url: "floorguide.php",
				success: function(data){
					setTimeout(function() {
						$("#inner-main-container").html(data);
						$("#inner-main-container").removeClass().addClass('floorguide-page fadeIn animated');
						$('body').removeClass().addClass('floorguide');
						floorguide();	
					}, 1000);
				}
			});
		}
	});
	//Directory Menu
	$('.menu-block.directory').on('click', function(){
		if( !$('#inner-main-container').hasClass('directory-page') ){
			$("#inner-main-container").removeClass('fadeIn animated').addClass('fadeOut animated');
			$('.keyboard-wrapper').removeClass('active');
			$.ajax({
				url: "directory.php",
				success: function(data){
					setTimeout(function() {
						$("#inner-main-container").html(data);			
						$("#inner-main-container").removeClass().addClass('directory-page fadeIn animated');
						$('body').removeClass().addClass('directory');
						directory();
					}, 1000);
				}
			});
		}
	});
	//Happening Menu
	$('.menu-block.happening').on('click', function(){
		if( !$('#inner-main-container').hasClass('happening-page') ){
			$("#inner-main-container").removeClass('fadeIn animated').addClass('fadeOut animated');
			$.ajax({
				url: "happening.php",
				success: function(data){
					setTimeout(function() {
						$("#inner-main-container").html(data);
						$("#inner-main-container").removeClass().addClass('happening-page fadeIn animated');
						$('body').removeClass().addClass('happening');
						happening();
					}, 1000);
				}
			});
		}
	});
	//catalogue Menu
	$('.menu-block.catalogue').on('click', function(){
		if( !$('#inner-main-container').hasClass('catalogue-page') ){
			$("#inner-main-container").removeClass('fadeIn animated').addClass('fadeOut animated');
			$.ajax({
				url: "catalogue.php",
				success: function(data){
					setTimeout(function() {
						$("#inner-main-container").html(data);
						$("#inner-main-container").removeClass().addClass('catalogue-page fadeIn animated');
						$('body').removeClass().addClass('catalogue');
						catalogue();
					}, 1000);
				}
			});
		}
	});

// Keyboard
// Footer
$('#input-search').click(function(){
	if( !$('#inner-main-container').hasClass('directory-page') ){
		$("#inner-main-container").removeClass('fadeIn animated').addClass('fadeOut animated');
		$.ajax({
			url: "directory.php",
			success: function(data){
				setTimeout(function() {
					$("#inner-main-container").html(data);			
					$("#inner-main-container").removeClass().addClass('directory-page fadeIn animated');
					$('body').removeClass().addClass('directory');
					directory();
				}, 1000);
			}
		});
	}
	$('footer .menu-block').removeClass('active');
	$('footer .menu-block.directory').addClass('active');

	$('.keyboard-wrapper').addClass('active');
	$('.btn-close-keyboard').removeClass('slideOutDown animated').addClass('slideInUp animated');
	$('footer .top').addClass('active');
});

$('.keyboard-wrapper .btn-close-keyboard').click(function(){
	$('.keyboard-wrapper').removeClass('active');
	$('.btn-close-keyboard').removeClass('slideInUp animated').addClass('slideOutDown animated');
	$('footer .top').removeClass('active');
});

$('#keyboard').jkeyboard({
	layout: "english_capital",
	input: $('#input-search'),
	customLayouts: {
		selectable: ["english_capital"],
		english_capital: [
		['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'backspace'],
		['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P', ],
		['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L'],
		['Z', 'X', 'C', 'V', 'B', 'N', 'M', 'clear'],
		['space']
		],
	}
});


// Footer Menu
$('footer .menu-block').click(function(){
	if( !$(this).hasClass('active') ){
		$('footer .menu-block').removeClass('active');
		$(this).addClass('active');
	}
});

$('footer .menu-block.home').click();

});