
<div id="content-top-container" class="container">

	<div class="container" id="transport-content-body">
		<div id="transport" class="tab-body">
			<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d3988.8038213061327!2d103.84751631557872!3d1.2920998990575956!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sfunan+it+mall!5e0!3m2!1sen!2ssg!4v1543222894139" width="100%" height="650" frameborder="0" style="border:0" allowfullscreen></iframe>

			<div class="inner-transport-body">
				<div class="row clearfix">
					<h1> <img src="./asset/icons/bus-icon.jpg" />  Bus Services </h1>
					<p class="block-ml90 mt-2 mb-5"> N1, N2, N3, N4, N5, 51, 61, 63, 80, 124, 145, 166, 174, 174e, 197, 851, 851e, 961, 961c, NR1, NR2, NR5, NR6, NR7, NR8</p>
				</div>
				<div class="row clearfix">
					<h1> <img src="./asset/icons/mrt-icon.jpg" /> Subway Services </h1>
					<p class="block-ml90 mt-2 mb-5"> City Hall East West Line (EW13), North South Line (NS25), Clarke Quay North East Line (NE5)</p>
				</div>
			</div>
		</div>

		<div id="carpark" class="tab-body" style="display: none;">
			<div class="carpak-top">
				<div id="carpark-info">
					<img src="./asset/carpark/carpark-info.jpg" />
				</div>
				<div id="result-slider" style="display: none">
					<div class="slider">
						<img src="./asset/carpark/result-image.jpg" class="ml-3 mr-3"/>
						<img src="./asset/carpark/result-image.jpg" class="ml-3 mr-3"/>
						<img src="./asset/carpark/result-image.jpg" class="ml-3 mr-3"/>
					</div>
				</div>
			</div>

			<div class="carpark-bottom">

				<div class="text-center">
					<input type="text" name="search" id="carpark-input-search" placeholder="Car Search" class="text-center" />
					<p class="mt-5 mb-5"> Enter your vehicle number </p>
					<div class="keyboard-wrapper">
						<div id="carpark-keyboard"> </div>
					</div>
				</div>
			</div>
		</div>

		<div class="tab-menu-box">
			<div class="row border">
				<div class="col-6 tab-block active" data-target="#transport">
					<a class="text-center" > TRANSPORT </a>
				</div>
				<div class="col-6 tab-block" data-target="#carpark" style="margin-left: 6px;">
					<a class="text-center"> CARPARK WAYFINDING </a>
				</div>
			</div>
		</div>

	</div>
</div>